--[[
	customPressure_flat_2.bs
]]

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"angle", 0, 100, 0},
		{"size_pMin", 0, 100, 0},
		{"size_pMax", 0, 100, 0},
		{"size_p", 0, 100, 25},
		{"size_rev.p", 0, 1, 0},
		{"alpha_pMin", 0, 100, 0},
		{"alpha_p", 0, 100, 0},
		{"alpha_rev_pMin", 0, 100, 0},
		{"alpha_rev.p", 0, 100, 0},
		{"random", 0, 100, 0}
	}

	pm.ja={
		"角度", "サイズ_p最小", "サイズ_p最大", "サイズ_p", "サイズ_rev.p", "透過_p最小",
		"透過_p", "透過_rev.p最小", "透過_rev.p", "ランダム"
	}

	for i in ipairs(pm) do

		local function eval(s, ...)
			assert(loadstring(string.format(s, ...)))()
		end

		if bs_lang() == "ja" then
			if pm.ja and pm.ja[i] then
				pm[i][1]=pm.ja[i]
			end
		end

		eval('param%d=setParam(pm[%d])', i, i)
		eval('pm[%d].v=bs_param%d()', i, i)

	end

	default_size=setParam(15, 0)

	bs_setmode(1)

	firstDraw=true
	sm={}

function drawRect(x, y, w, h, angle, r, g, b, a)

	bs_polygon(-0.5, -0.5)
	bs_polygon(0.5, -0.5)
	bs_polygon(0.5, 0.5)
	bs_polygon(-0.5, 0.5)

	bs_polygon_mul(w, h)
	bs_polygon_rotate(angle)
	bs_polygon_move(x, y)

	bs_fill(r, g, b, a)

	return
end

-- main --
function main(x, y, p)

	local function smPut(array, num, value)

		if #array == 0 then
			for i=1, math.max(num, 1) do
				array[i]={}
				array[i]=value
			end
		else
			table.insert(array, value)
			table.remove(array, 1)
		end

	end

	if firstDraw then
		lastX, lastY=x, y
		lastRad=0
	end

	local wMin=bs_width_min()
	local wMax=bs_width_max()
	local dx, dy=bs_dir()

	local temp=0
	local pressSize=1

	if pm[4].v ~= 0 then

		temp=p

		if pm[5].v == 1 then
			temp=1-temp
		end

		pressSize=temp^(8*pm[4].v/100)

	end

	if pm[3].v ~= 0 and pm[5].v == 0 then
		pressSize=math.min(pressSize*(1-pressSize^(pm[3].v/100))+pressSize^(1-pm[3].v/100*0.75), 1)
	end

	local pLimit=wMin/wMax

	if pm[2].v ~= 0 then

		temp=p^(2*pm[2].v/100)

		if pm[5].v == 1 then
			temp=p^(8*pm[2].v/100)
		end

		pLimit=pLimit*temp

	end

	if pm[5].v == 1 then
		pressSize=pressSize*(1-pm[3].v/100)
	end

	pressSize=pressSize*(1-pLimit)+pLimit

	local pressAlpha=1

	if pm[7].v ~= 0 then
		pressAlpha=p^(4*pm[7].v/100)*(1-pm[6].v/100)+pm[6].v/100
	end

	if pm[9].v ~= 0 then
		pressAlpha=pressAlpha*((1-p)^(math.min(pm[9].v, 50)/50+3*math.max(pm[9].v-50, 0)/50)*(1-pm[8].v/100)+pm[8].v/100)
	end

	local w=wMax*pressSize
	local distance=bs_distance(lastX-x, lastY-y)

	local intervalAdjust=wMax-(wMax-w)*(1-pressSize)
	local interval=math.max(intervalAdjust*0.08, 1)

	if not firstDraw then
		if distance < interval then
			return 0
		end
	end

	local pressRandom=math.random(100-pm[10].v, 100)/100

	local r, g, b=bs_fore()
	local a=bs_opaque()*255*pressAlpha

	if pressAlpha ~= 1 then
		a=a*(pressRandom^math.random())
	end

	smPut(sm, math.floor(15^(1-pm[10].v/100)*(1-pressSize))+1, w)

	local ww=0
	local divWeight=0

	for i=1, #sm do

		local weight=i^8

		ww=ww+sm[i]*weight
		divWeight=divWeight+weight

	end

	ww=ww/divWeight*pressRandom

	local cnt=distance/interval

	if ww <= 1 then
		cnt=math.max(cnt, 0.5)
	end

	local wwLimit=1-pm[6].v/100

	if ww < wwLimit then
		a=a*(pressSize^((wwLimit-ww)/wwLimit))
		ww=math.max(ww, wwLimit)
	end

	local midX, midY=x-distance%interval*dx, y-distance%interval*dy

	local angle=math.rad(180*pm[1].v/100)

	if pm[1].v == 100 then
		angle=angle+math.rad(bs_grand(0, 180))
	end

	local rad={bs_atan(lastX-x, lastY-y), bs_atan(midX-x, midY-y)}
	local ddx, ddy=math.cos(rad[1]), math.sin(rad[1])

	if math.abs(lastRad-rad[1]) > math.rad(45) then
		ddx, ddy=math.cos(rad[2]), math.sin(rad[2])
	end

	local xx, yy=midX+interval*ddx*cnt, midY+interval*ddy*cnt

	local flat=0.5
	local dis=bs_distance(xx-x, yy-y)

	xx=xx-dis*flat*ddx
	yy=yy-dis*flat*ddy

	angle=angle+bs_atan(ddx, ddy)

	if not firstDraw then

		local dis=bs_distance(xx-x, yy-y)/math.max(1-flat, 0.01)
		local cx, cy=dis/2*ddx, dis/2*ddy

		if math.max(pm[10].v/100-0.5, 0)/0.5 < math.random() then

			drawRect(xx-cx, yy-cy, ww*flat, ww*0.99, angle, r, g, b, a)
			drawRect(midX, midY, math.min(ww*flat, dis), ww*0.99, angle, r, g, b, a)

		end

		drawRect(x+cx, y+cy, ww*flat, ww*0.99, angle, r, g, b, a)

	end

	firstDraw=false
	lastX, lastY=midX, midY
	lastRad=rad[1]

	return 1

end
